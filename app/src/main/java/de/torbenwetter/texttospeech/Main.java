package de.torbenwetter.texttospeech;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.readystatesoftware.viewbadger.BadgeView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class Main extends AppCompatActivity {

    private static final Point screenSize = new Point();
    private static Bitmap backgroundBitmap = null;

    private EditText input;

    private static Map<Integer, TextView> messageCache = new HashMap<>();

    private static Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        final Display display = getWindowManager().getDefaultDisplay();
        display.getSize(screenSize);

        if (backgroundBitmap == null)
            backgroundBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.background), screenSize.x, screenSize.y, true);
        final RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
        layout.setBackground(new BitmapDrawable(getResources(), backgroundBitmap));

        final Button send = (Button) findViewById(R.id.send);
        send.setTypeface(Typeface.createFromAsset(getAssets(), "roboto-thin.ttf"));
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input != null)
                    if (!input.getText().toString().trim().isEmpty())
                        sendMessage();
            }
        });

        input = (EditText) findViewById(R.id.input);
        input.setTypeface(Typeface.createFromAsset(getAssets(), "roboto-thin.ttf"));
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void afterTextChanged(Editable editable) {
                send.setEnabled(!input.getText().toString().trim().isEmpty());
            }
        });
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    send.performClick();
                    return true;
                }
                return false;
            }
        });

        connectToSocket();
    }

    private void sendMessage() {
        final TextView message = new TextView(this);
        final LinearLayout.LayoutParams messageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final Integer space = (int) dpToPixel(5);
        messageParams.setMargins(space, 0, space, space);
        message.setLayoutParams(messageParams);

        final String text = input.getText().toString().trim();
        message.setText(text);
        message.setTextSize(dpToPixel(6));
        message.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
        message.setMinWidth((int) dpToPixel(67));
        message.setTypeface(Typeface.createFromAsset(getAssets(), "roboto-thin.ttf"));
        message.setTextColor(ContextCompat.getColor(this, R.color.colorText));
        message.setBackground(ContextCompat.getDrawable(this, R.drawable.background_red));
        message.setPadding(space, space, space, (int) (dpToPixel(5) * 3.5));

        final Integer id = getNewId();
        messageCache.put(id, message);

        final LinearLayout messages = (LinearLayout) findViewById(R.id.messages);
        messages.addView(message);

        final BadgeView badgeView = new BadgeView(this, message);
        badgeView.setBadgePosition(BadgeView.POSITION_BOTTOM_RIGHT);
        badgeView.setBackground(ContextCompat.getDrawable(this, R.drawable.background_badge));
        badgeView.setTextColor(ContextCompat.getColor(this, R.color.colorText));
        badgeView.setTextSize(dpToPixel(2));
        badgeView.setTypeface(Typeface.createFromAsset(getAssets(), "roboto-thin.ttf"));
        badgeView.setPadding(0, (int) dpToPixel(2), 0, 0);
        final String time = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss", Locale.GERMANY).format(Calendar.getInstance().getTime());
        badgeView.setText(time);
        badgeView.show();

        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.scrollTo(0, scrollView.getBottom());

        input.setText("");

        if (socket == null || !socket.connected()) {
            Toast.makeText(this, getResources().getString(R.string.message_not_sent), Toast.LENGTH_SHORT).show();
            return;
        }

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(String.valueOf(id), text);
            socket.emit("message", jsonObject);
            message.setBackground(ContextCompat.getDrawable(this, R.drawable.background_yellow));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private Integer getNewId() {
        int id;
        do {
            id = new Random().nextInt(9000000) + 1000000;
        } while (messageCache.containsKey(id));
        return id;
    }

    private float dpToPixel(float dp) {
        final float density = getResources().getDisplayMetrics().density;
        return dp * (density == 1.0f || density == 1.5f || density == 2.0f ? 3.0f : density) + 0.5f;
    }

    private boolean isWifiConnected() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    private String getWifiName() {
        final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        return wifiManager.getConnectionInfo().getSSID();
    }

    private void connectToSocket() {
        if (!isWifiConnected() || !getWifiName().equals(getResources().getString(R.string.wlan_name))) {
            final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.not_in_wlan), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    connectToSocket();
                }
            });
            snackbar.show();
            return;
        }

        try {
            socket = IO.socket("http://192.168.178.52:4040/");
            socket.connect();

            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    // connected
                }
            }).on("success", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    final JSONObject data = (JSONObject) args[0];
                    try {
                        final int id = data.getInt("id");
                        final TextView message = messageCache.get(id);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                message.setBackground(ContextCompat.getDrawable(Main.this, R.drawable.background_green));
                            }
                        });
                        messageCache.remove(id);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }
    }
}
